import React from 'react'

const Splash = () => (
    <div className="ascii splash">
        <div> _                 _</div>
        <div>| |               | |</div>
        <div>| |__  _   _ _ __ | |_ ___ _ __ </div>
        <div>| '_ \| | | | '_ \| __/ _ \ '__|</div>
        <div>| | | | |_| | | | | ||  __/ |</div>
        <div>|_| |_|\__,_|_| |_|\__\___|_|</div>
        <div>               _ _</div>
        <div>              | | |</div>
        <div> ___  __ _  __| | | ___ _ __</div>
        <div>/ __|/ _` |/ _` | |/ _ \ '__|</div>
        <div>\__ \ (_| | (_| | |  __/ |</div>
        <div>|___/\__,_|\__,_|_|\___|_|</div>
        <div className="suggestion">
            <div>Version: { applicationVersion }</div>
            <div>Type 'help' to being your adventure</div>
        </div>
    </div>
)

export default Splash