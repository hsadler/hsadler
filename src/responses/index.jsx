import React from 'react'
import Blogs from '../blogs'
import ContactMe from './contactMe/contactMe'
import FeelsBadMan from './feelsBadMan'
import Hunter from './hunter/hunter'
import Projects from '../projects'
import Resume from './resume/resume'
import Thinking from './thinking/thinking'
import Twiggle from './twiggle/twiggle'
import Splash from './splash/splash'

class ResponseRouter extends React.Component {
    constructor(props) {
        super(props)

        this.findCorrectResponse = this.findCorrectResponse.bind(this)

        this.responses = {
            "blogs": <Blogs/>,
            "contactme": <ContactMe/>,
            "feelsbadman": <FeelsBadMan/>,
            "'help'": <HelpTease/>,
            "hunter": <Hunter/>,
            "projects": <Projects/>,
            "resume": <Resume/>,
            "splash": <Splash/>,
            "thinking": <Thinking/>,
            "twiggle": <Twiggle/>,
        }

        this.commands = [...Object.keys(this.responses), "cls", "clear", "help"].sort().join(" ")
    }

    findCorrectResponse() {
        const cmd = this.props.cmd.toLowerCase()

        if (cmd === "help") {
            return <Help commands={this.commands}/>
        } else if (this.responses.hasOwnProperty(cmd)) {
            return this.responses[cmd]
        } else if (cmd.includes('blogs')) {
            const cmdSplit = cmd.split(" ")
            const id = cmdSplit[cmdSplit.length - 1]
            return <Blogs id={id}/>
        } else if (cmd.includes('projects')) {
            const cmdSplit = cmd.split(" ")
            const id = cmdSplit[cmdSplit.length - 1]
            return <Projects id={id}/>
        } else {
            return <Unknown/>
        }
    }

    render() {
        return this.findCorrectResponse()
    }
}

const Help = ({ commands }) => <div>Available Commands: { commands }</div>
const HelpTease = () => <div>You don't need the quotes 😉</div>
const Unknown = () => <div>If you are looking for guidance, 'help' will guide you!</div>

export default ResponseRouter