import React from 'react'

const Twiggle = () => (
    <div className="twigglePanel">
        <img className="picture" src="/images/twiggleInChair.png"/>
        <div className="content">
            <div><span>Name:</span> Twiggle</div>
            <div><span>Age:</span> 6</div>
            <div><span>Size:</span> 20lbs</div>
            <div><span>Interests</span></div>
            <ul>
                <li>Cuddling</li>
                <li>Eating</li>
                <li>Knocking Over Water</li>
                <li>Sleeping</li>
                <li>Tugging Mouse Cords</li>
            </ul>
            <div>
                <div><span>Special Notes</span></div>
                <p>The vet says he is part maine coon but i'm not sure.</p>
                <p>Sometimes he plays fetch, sometimes he doesn't.</p>
                <p>The vet also said hes at his target weight, but i'm also not sure about that.</p>
            </div>
        </div>
    </div>
)

export default Twiggle