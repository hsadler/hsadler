import React from 'react'

const ResumeSection = ({ description, name, position, time }) => (
    <div className="resumeSection">
        <div className="name">{ name }</div>
        <div className="information">{ position }, { time }</div>
        <div className="description">
            <ul>{ description.map((snippet, index) => <li key={index}>{ snippet }</li>) }</ul>
        </div>
    </div>
)

export default ResumeSection