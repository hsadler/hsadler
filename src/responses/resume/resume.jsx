import React from 'react'
import ResumeSection from './resumeSection'

class Resume extends React.Component {
    constructor(props) {
        super(props)

        this.workSections = [
            {
                description: [
                    "Developed 13 applications impacting ~10,000 internal and external users", 
                    "Collaboratively designed a high volume transaction based document generation system", 
                    "Analyzed and created enhancements for multiple large code-base legacy applications",
                    "Mentored 5 new team members, aiding in their transition to the Shelter culture",
                    "Focused on priority business needs with the ability to shift work streams as needed",
                    "Leveraged testing during all parts of the Agile SDLC, starting from a spotlight on writing testable code to creation and execution of tests",
                ],
                name: "Shelter Insurance",
                position: "Software Developer 3",
                time: "April 2016 - Present",
            }, {
                description: [
                    "Modernized existing PHP based website by transitioning to WordPress CMS framework",
                    "Improved user experience through implementation of responsive features into existing content",
                    "Designed and implemented mobile-only features to drive quick and meaningful interactions with users",
                ],
                name: "University Of Missouri - Ellis Library",
                position: "Student Programmer",
                time: "May 2013 - May 2015",
            }, {
                description: [
                    "Actively listened to customers’ needs and made recommendations appropriate to their individual context",
                    "Helped to welcome and develop new team members",
                    "Awarded ‘Sales Associate of the Month’ July 2010"
                ],
                name: "Best Buy",
                position: "Computer Sales Associate",
                time: "September 2009 - January 2012",
            }
        ]

        this.college = {
            description: ["Programming focused degree from Mizzou's Engineering School"],
            name: "University Of Missouri",
            position: "Bachelor of Science, Information Technology",
            time: "August 2011 - May 2016",
        }
    }

    render() {
        return (
            <div className="resume">
                <div className="opener">Before reading all the information, I specialize in Java (Spring Boot), Javascript (Express, React, Redux), and Lua (World of Warcraft)</div>
                <div className="title">Education</div>
                <ResumeSection { ...this.college } />
                <div className="title">Work</div>
                { this.workSections.map((workSection, index) => <ResumeSection key={index} { ...workSection }/>) }
                <div className="ps">Download <a href="/static/Resume.pdf">PDF</a></div>
                <div className="ps">For code examples, or contact information enter 'contactme', and for information about key projects enter 'projects'</div>
            </div>
        )
    }
}
    

export default Resume