import React from 'react'

const ContactMe = () => (
    <div className="contactMe">
        <div><i className="fa fa-envelope-o" aria-hidden="true"></i><a href="mailto:auro@hsadler.com">auro@hsadler.com</a></div>
        <div><i className="fa fa-gitlab" aria-hidden="true"></i><a href="https://gitlab.com/hsadler">HSadler (Newer Content)</a></div>
        <div><i className="fa fa-github" aria-hidden="true"></i><a href="https://github.com/Aurokin">Aurokin (Older Content)</a></div>
        <div><i className="fa fa-twitch" aria-hidden="true"></i><a href="https://www.twitch.tv/Auro_">Auro_</a></div>
        <div><i className="fa fa-youtube-play" aria-hidden="true"></i><a href="https://www.youtube.com/channel/UCosyLzFr_tu7MkC7vghtLnQ?view_as=subscriber">Auro</a></div>
    </div>
)

export default ContactMe