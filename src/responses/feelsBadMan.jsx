import React from 'react'

const FeelsBadMan = () => (
    <div className="ascii feelsBadMan">
        <div>          ████████     ██████</div>
        <div>         █░░░░░░░░██ ██░░░░░░█</div>
        <div>        █░░░░░░░░░░░█░░░░░░░░░█</div>
        <div>       █░░░░░░░███░░░█░░░░░░░░░█</div>
        <div>       █░░░░███░░░███░█░░░████░█</div>
        <div>      █░░░██░░░░░░░░███░██░░░░██</div>
        <div>     █░░░░░░░░░░░░░░░░░█░░░░░░░░███</div>
        <div>    █░░░░░░░░░░░░░██████░░░░░████░░█</div>
        <div>    █░░░░░░░░░█████░░░████░░██░░██░░█</div>
        <div>   ██░░░░░░░███░░░░░░░░░░█░░░░░░░░███</div>
        <div>  █░░░░░░░░░░░░░░█████████░░█████████</div>
        <div> █░░░░░░░░░░█████ ████   ████ █████   █</div>
        <div> █░░░░░░░░░░█      █ ███  █     ███ █   █</div>
        <div>█░░░░░░░░░░░░█   ████ ████    ██ ██████</div>
        <div>░░░░░░░░░░░░░█████████░░░████████░░░█</div>
        <div>░░░░░░░░░░░░░░░░█░░░░░█░░░░░░░░░░░░█</div>
        <div>░░░░░░░░░░░░░░░░░░░░██░░░░█░░░░░░██</div>
        <div>░░░░░░░░░░░░░░░░░░██░░░░░░░███████</div>
        <div>░░░░░░░░░░░░░░░░██░░░░░░░░░░█░░░░░█</div>
        <div>░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░█</div>
        <div>░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░█</div>
        <div>░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░█</div>
        <div>░░░░░░░░░░░█████████░░░░░░░░░░░░░░██</div>
        <div>░░░░░░░░░░█▒▒▒▒▒▒▒▒███████████████▒▒█</div>
        <div>░░░░░░░░░█▒▒███████▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█</div>
        <div>░░░░░░░░░█▒▒▒▒▒▒▒▒▒█████████████████</div>
        <div>░░░░░░░░░░████████▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█</div>
        <div>░░░░░░░░░░░░░░░░░░██████████████████</div>
        <div>░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░█</div>
        <div>██░░░░░░░░░░░░░░░░░░░░░░░░░░░██</div>
        <div>▓██░░░░░░░░░░░░░░░░░░░░░░░░██</div>
        <div>▓▓▓███░░░░░░░░░░░░░░░░░░░░█</div>
        <div>▓▓▓▓▓▓███░░░░░░░░░░░░░░░██</div>
        <div>▓▓▓▓▓▓▓▓▓███████████████▓▓█</div>
        <div>▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓██</div>
        <div>▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓█</div>
        <div>▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓█</div>
    </div>
)

export default FeelsBadMan