import React from 'react'

const Hunter = () => (
    <div className="hunter">
        <img className="picture" src="/images/hunterBlizzcon2017.png"/>
        <div className="content">
            <div className="opener">
                <p>Hey! My names Hunter Sadler, or Auro, I'm 24 years old.</p>
                <p>Currently I live in Columbia Missouri, but I'm from the northwest suburbs of Chicago.</p>
                <p>I have a cat named twiggle, who you can check out in another command</p>
                <p>I love programming, playing video games, and spending time with friends.</p>
            </div>
            <div className="programming">
                <p>At work I spend most of my time programming in Java, and sometimes Groovy, with Spring Boot.</p>
                <p>It took a very long time for Java to grow on me, but I'm finally beginning to enjoy the structure.</p>
                <p>At home most of my programming is done using NodeJS, and React with Redux.</p>
                <p>I think there is a place for most languages, and its all about making the right decision for the project and team.</p>
                <p>I also work with Lua for World of Warcraft AddOn and WeakAura development.</p>
            </div>
            <div className="videoGames">
                <p>I enjoy playing games of all types, but my favorites are Blizzard and Nintendo games.</p>
                <p>World of Warcraft has been a part of my life since 2005, and I've raided with <a href="http://healerchat.com/#/">HC</a> since WoD.</p>
                <p>During WoD I used to push CM times, mostly playing Blood DK, Brewmaster Monk, and Hunter</p>
                <p>In Legion I mostly focus on Raiding, as an Officer in <a href="http://healerchat.com/#/">HC</a> but I also wrote <a href="https://www.curseforge.com/wow/addons/gottagofast">GottaGoFast</a></p>
                <p>For raiding i've played Balance Druid since Highmaul but as raid group we finished ToS finished at US 26th.</p>
                <p>In the past i've also competitevely played Melee, Project M, and Speedran OOT.</p>
            </div>
        </div>
    </div>
)

export default Hunter