import React from 'react'
import ReactDOM from 'react-dom'
import ReactGA from 'react-ga'
import { Provider } from 'react-redux'
import { applyMiddleware, combineReducers, compose, createStore } from 'redux'
import createHistory from 'history/createHashHistory'
import { Route } from 'react-router'
import { ConnectedRouter, routerReducer, routerMiddleware } from 'react-router-redux'
import thunk from 'redux-thunk'
import AppWrapper from './wrappers/appWrapper'
import Reducers from './reducers'
require('./components/app/app.scss')

ReactGA.initialize('UA-111882274-1')
const history = createHistory()
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const store = createStore(combineReducers({ ...Reducers, router: routerReducer }), composeEnhancers(applyMiddleware(thunk, routerMiddleware(history))))

ReactDOM.render(<Provider store={store}><ConnectedRouter history={history}><Route component={AppWrapper} path="/"/></ConnectedRouter></Provider>, document.getElementById("app"))