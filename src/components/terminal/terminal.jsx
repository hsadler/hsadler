import React from 'react'
import CommandInputWrapper from '../../wrappers/commandInputWrapper'
import InputLine from '../inputLine/inputLine'
import ResponseRouter from '../../responses'

class Terminal extends React.Component {
    constructor(props) {
        super(props)

        this.scrollToBottom = this.scrollToBottom.bind(this)
    }

    scrollToBottom() {
        this.hb.scrollIntoView({ behavior: "smooth" })
    }

    componentDidUpdate() {
        this.scrollToBottom()
    }

    render() {
        const display = []
        this.props.sentCommands.forEach(({ id, command, hidden }) => {
            display.push(<InputLine key={id} commandInput={false} content={command} hidden={hidden}/>)
            display.push(<ResponseRouter key={`${id}r`} cmd={command}/>)
        })

        return(
            <div className="terminal">
                { display }
                <CommandInputWrapper/>
                <div ref={hb => { this.hb = hb }}></div>
            </div>
        )
    }
}

export default Terminal