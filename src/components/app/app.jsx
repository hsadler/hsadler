import React from 'react'
import ReactGA from 'react-ga'
import TerminalWrapper from '../../wrappers/terminalWrapper'

class App extends React.Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        ReactGA.pageview(window.location.hash)

        const path = this.props.location.pathname
        if (path !== "/") {
            this.props.sendCommand(path.replace(/\//g, " ").trim())
        }
    }

    render() {
        return(
            <div className="appWrapper" onClick={this.props.refocus}>
                <TerminalWrapper/>
            </div>
        )
    }
}

export default App