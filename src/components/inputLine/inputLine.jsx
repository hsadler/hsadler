import React from 'react'

class InputLine extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        const content = this.props.commandInput ? <ActiveLine content={this.props.content} refocusCount={this.props.refocusCount} submitCommand={this.props.submitCommand} updateInput={this.props.updateInput}/> : <div className="content">{ this.props.content }</div>

        return (
            <div className={`inputLine ${this.props.hidden ? 'hidden' : ''}`}>
                <div className="prefix">
                    <span className="location">~</span>
                    <span className="userType">$</span>
                </div>
                { content }
            </div>
        )
    }
}

class ActiveLine extends React.Component {
    constructor(props) {
        super(props)
        this.submit = this.submit.bind(this)
    }

    componentDidMount() {
        this.input.focus()
    }

    componentDidUpdate() {
        this.input.focus()
    }

    submit(e) {
        e.preventDefault()
        this.props.submitCommand()
    }

    render() {
        return (
            <form onSubmit={this.submit}>
                <input 
                    className="content"
                    onChange={this.props.updateInput}
                    ref={input => { this.input = input }}
                    value={this.props.content}/>
            </form>
        )
    }
}

export default InputLine