import React from 'react'
import InputLine from './inputLine'
import Renderer from 'react-test-renderer'

test('Input Line Hides On Prop', () => {
    const hiddenInput = Renderer.create(<InputLine hidden={true} commandInput={false}/>)
    const json = hiddenInput.toJSON()
    expect(json).toMatchSnapshot()
})

test('Input Line Displays Content In Div For None Command Inputs', () => {
    const commandLineInput = Renderer.create(<InputLine hidden={false} commandInput={false} content="Hi Ya!"/>)
    const json = commandLineInput.toJSON()
    expect(json).toMatchSnapshot()
})