import React from 'react'

const BlogPost = ({ content, date, title}) => (
    <div className="blogPost">
        <div className="title">{ title }</div>
        { content }
        <div className="date">Hunter Sadler, { date }</div>
    </div>
)

export default BlogPost