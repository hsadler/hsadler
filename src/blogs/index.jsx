import React from 'react'
import BlogPost from './blogPost'
import HelloWorld from './helloWorld'

class Blogs extends React.Component {
    constructor(props) {
        super(props)

        this.blogPosts = {
            "1": HelloWorld
        }

        this.findPost = this.findPost.bind(this)
    }

    findPost() {
        if (this.blogPosts.hasOwnProperty(this.props.id)) {
            const post = this.blogPosts[this.props.id]
            return <BlogPost content={post.content} date={post.date} title={post.title}/>
        } else {
            return <BlogSplash blogPosts={this.blogPosts}/>
        }
    }

    render() {
        return this.findPost()
    }
}

const BlogSplash = ({ blogPosts }) => {
    const posts = Object.keys(blogPosts)
        .map(index => ({ ...blogPosts[index], id: index }))
        .map(blogPost => <div key={blogPost.title}>{blogPost.id}) {blogPost.title}</div>)

    return (
        <div>
            { posts }
            <div>Enter Blog Id With Command, Example 'blogs 1'</div>
        </div>
        
    )
}

export default Blogs