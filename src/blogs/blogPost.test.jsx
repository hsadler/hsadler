import React from 'react'
import BlogPost from './blogPost'
import Renderer from 'react-test-renderer'

import HelloWorld from './helloWorld'

const createMock = post => Renderer.create(<BlogPost content={post.content} date={post.date} title={post.title}/>)
const posts = [HelloWorld]

posts.forEach(post => {
    test(`${post.title} Renders Properly`, () => {
        const postComponent = createMock(post)
        const json = postComponent.toJSON()
        expect(json).toMatchSnapshot()
    })
})