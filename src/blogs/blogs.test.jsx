import React from 'react'
import Blogs from './index'
import Renderer from 'react-test-renderer'

test('Blog Post Renders Valid Post', () => {
    const blogsComponent = Renderer.create(<Blogs id={1}/>)
    const json = blogsComponent.toJSON()
    expect(json).toMatchSnapshot()
})

test('Blog Post Renders Help For Invalid Id', () => {
    const blogsComponent = Renderer.create(<Blogs id={0}/>)
    const json = blogsComponent.toJSON()
    expect(json).toMatchSnapshot()
})

test('Blog Post Renders Help For Missing Id', () => {
    const blogsComponent = Renderer.create(<Blogs/>)
    const json = blogsComponent.toJSON()
    expect(json).toMatchSnapshot()
})