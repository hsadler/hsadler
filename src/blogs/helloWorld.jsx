import React from 'react'

const content = <div className="content"><p>Hey my name's Hunter Sadler and this is my first post</p></div>

const HelloWorld = {
    content,
    date: "12/27/2017",
    title: "Hello World"
}

export default HelloWorld