import { createSelector } from 'reselect'

const currentCommandSelector = state => state.currentCommand
const refocusCountSelector = state => state.refocusCount
const sentCommandsSelector = state => state.sentCommands

export {
    currentCommandSelector,
    refocusCountSelector,
    sentCommandsSelector,
}