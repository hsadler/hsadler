import React from 'react'

const Project = ({ code, content, title, url }) => (
    <div className="project">
        <div className="title"><a href={ url }>{ title }</a> <a href={ code }><i className="fa fa-gitlab" aria-hidden="true"></i></a></div>
        { content }
    </div>
)

export default Project