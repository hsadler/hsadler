import React from 'react'

const content = <div className="content">
    <p>HC Toolkit is HC's WoW Raiding Addon</p>
    <p>The Addon is coded in Lua on top of Ace3</p>
    <p>It maintains group information, like each players unit ID, name, class, and spec</p>
    <p>This makes it easy to add additional boss modules without re-writing boilerplate code like you typically would with custom WeakAuras</p>
    <p>Some notable uses are Maiden Soak Assignments, Avatar Soak Assignments, KJ Soak Tracking, and Argus Burst / Bomb Marking</p>
    <p>Antorus did not require much custom code!</p>
    <p>At the moment this project has been placed on hold, it may be revived at a later date!</p>
</div>

const HcToolkit = {
    code: "https://gitlab.com/hsadler/hc-toolkit",
    content,
    title: "HC Toolkit",
    url: "https://www.curseforge.com/wow/addons/hctoolkit"
}

export default HcToolkit