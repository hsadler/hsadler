import React from 'react'

const content = <div className="content">
    <p>Healerchat.com is the main website for HC</p>
    <p>HC is a WoW raiding guild on Burning Legion US</p>
    <p>It features support for Blog Posts, Applications, Roster, and Recruitment</p>
    <p>Backend: NodeJS, Express, Sequelize</p>
    <p>Frontend: React, Redux, PivotalUI</p>
    <p>Its Battle.net integration is notable</p>
    <p>The site pulls character data from the Battle.net API</p>
    <p>This data is used for applications, and the roster</p>
</div>

const Healerchat = {
    code: "https://gitlab.com/hsadler/hc",
    content,
    title: "HealerChat.com",
    url: "http://healerchat.com",
}

export default Healerchat