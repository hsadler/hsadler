import React from 'react'
import Projects from './index'
import Renderer from 'react-test-renderer'

test('Blog Post Renders Valid Post', () => {
    const projectsComponent = Renderer.create(<Projects id={1}/>)
    const json = projectsComponent.toJSON()
    expect(json).toMatchSnapshot()
})

test('Blog Post Renders Help For Invalid Id', () => {
    const projectsComponent = Renderer.create(<Projects id={0}/>)
    const json = projectsComponent.toJSON()
    expect(json).toMatchSnapshot()
})

test('Blog Post Renders Help For Missing Id', () => {
    const projectsComponent = Renderer.create(<Projects/>)
    const json = projectsComponent.toJSON()
    expect(json).toMatchSnapshot()
})