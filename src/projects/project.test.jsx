import React from 'react'
import Project from './project'
import Renderer from 'react-test-renderer'

import GottaGoFast from './gottaGoFast'
import HcBot from './hcBot'
import HcToolkit from './hcToolkit'
import Healerchat from './healerchat'
import HearthQuiz from './hearthquiz'
import Hsadler from './hsadler'

const createMock = projectData => Renderer.create(<Project code={projectData.code} content={projectData.content} title={projectData.title} url={projectData.url}/>)
const projects = [GottaGoFast, HcBot, HcToolkit, Healerchat, HearthQuiz, Hsadler]

projects.forEach(projectData => {
    test(`${projectData.title} Renders Properly`, () => {
        const projectComponent = createMock(projectData)
        const json = projectComponent.toJSON()
        expect(json).toMatchSnapshot()
    })
})