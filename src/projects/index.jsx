import React from 'react'
import GottaGoFast from './gottaGoFast'
import HcBot from './hcBot'
import HcToolkit from './hcToolkit'
import Healerchat from './healerchat'
import HearthQuiz from './hearthquiz'
import HSadler from './hsadler'
import Project from './project'

class Projects extends React.Component {
    constructor(props) {
        super(props)

        this.projects = {
            "1": GottaGoFast,
            "2": HcBot,
            "3": HcToolkit,
            "4": Healerchat,
            "5": HearthQuiz,
            "6": HSadler,
        }

        this.findProject = this.findProject.bind(this)
    }

    findProject() {
        if (this.projects.hasOwnProperty(this.props.id)) {
            const project = this.projects[this.props.id]
            return <Project code={project.code} content={project.content} title={project.title} url={project.url}/>
        } else {
            return <ProjectsSplash projects={this.projects}/>
        }
    }

    render() {
        return this.findProject()
    }
}

const ProjectsSplash = ({ projects }) => {
    const projectsMapped = Object.keys(projects)
        .map(index => ({ ...projects[index], id: index }))
        .map(project => <div key={project.title}>{project.id}) {project.title}</div>)

    return (
        <div>
            { projectsMapped }
            <div>Enter Project Id With Command, Example 'projects 1'</div>
        </div>
    )
}

export default Projects