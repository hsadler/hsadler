import React from 'react'

const content = <div className="content">
    <p>GottaGoFast is a WoW Mythic+ AddOn, written in Lua on top of Ace3</p>
    <p>The original addon stems from a WeakAura I re-wrote during WoD</p>
    <p>After testing throughout Legion Beta it was one of the first Mythic+ AddOns available</p>
    <p>Its ability to count deaths, show +2/+3 timers, and display mob count in tooltips are all notable features</p>
    <p>However one of the most useful features to me is its ability to start the timer at 0, and record key event timings</p>
    <p>This data can be used when planning speedruns, or high pushes</p>
    <p>One of my favorite features is GottaGoFastHistory, /ggfh</p>
    <p>This feature records your runs, and displays all your information to be accessed anytime</p>
    <p>This feature also powers the "new record" text box at the start, and end of runs</p>
</div>

const GottaGoFast = {
    code: "https://gitlab.com/hsadler/gottagofast",
    content,
    title: "GottaGoFast",
    url: "https://www.curseforge.com/wow/addons/gottagofast",
}

export default GottaGoFast