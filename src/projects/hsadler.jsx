import React from 'react'

const content = <div className="content">
    <p>Well, this is awkard, you're already on HSadler.com</p>
    <p>Its used as a personal website, resume, blog and more, just type 'help'</p>
    <p>Its built on React, and Redux built together with Webpack and Babel</p>
    <p>Currently its hosted in an AWS S3 Bucket</p>
    <p>Go Explore!!!</p>
</div>

const HSadler = {
    code: "https://gitlab.com/hsadler/hsadler",
    content,
    title: "HSadler.com",
    url: "http://hsadler.com",
}

export default HSadler