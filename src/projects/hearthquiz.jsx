import React from 'react'

const content = <div className="content">
    <p>HearthQuiz.me is a flash card website for Hearthstone</p>
    <p>It consists of four components</p>
    <ul>
        <li><a href="http://hearthquiz.me">HearthQuiz FrontEnd</a>: React + Redux application used for displaying cards, and helping navigate the filters</li>
        <li><a href="http://api.hearthquiz.me">HearthQuiz BackEnd</a>: Spring Boot Java application that aggregates different APIs like HearthstoneJSON and filters the cards available</li>
        <li><a href="http://cardbacks.hearthquiz.me">HearthQuiz Cardbacks</a>: NodeJS script to download cardbacks from HearthHead and build a reference JSON file</li>
        <li><a href="http://images.hearthquiz.me">HearthQuiz Images</a>: Forked copy of HearthstoneJSON Image API, which modifies the URLs for hosting in S3</li>
    </ul>
    <p>This site is currently a WIP with only the FrontEnd considered unfinished</p>
    <p>Everything is hosted in AWS in either S3 Buckets or EC2 instances driven by ElasticBean</p>
    <p>This project has been placed on hold! It may be revived at a later date</p>
</div>

const HearthQuiz = {
    code: "https://gitlab.com/hearthquiz",
    content,
    title: "HearthQuiz.me",
    url: "http://hearthquiz.me",
}

export default HearthQuiz