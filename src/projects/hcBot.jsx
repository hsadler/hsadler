import React from 'react'

const content = <div className="content">
    <p>HC Bot is a Discord Bot</p>
    <p>Its primary purpose is to report new Applications from the HC Website</p>
    <p>HC Bot can also read messages in certain channels and reply with pre-programmed responses</p>
    <p>The HC website backend fills an amazon simple queue when ever a new application is submitted</p>
    <p>Every 5 minutes it reads the queue, and then creates a message in a special application discord channel</p>
</div>

const HcBot = {
    code: "https://gitlab.com/hsadler/hc-bot",
    content,
    title: "HC Bot",
    url: "#"
}

export default HcBot