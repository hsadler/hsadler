import { connect } from 'react-redux'
import InputLine from '../components/inputLine/inputLine'
import { currentCommandSelector, refocusCountSelector } from '../selectors'
import { newInput, submitCommand } from '../actions'

const mapStateToProps = state => ({
    commandInput: true,
    content: currentCommandSelector(state),
    refocusCount: refocusCountSelector(state),
})

const mapDispatchToProps = dispatch => ({
    submitCommand: () => dispatch(submitCommand()),
    updateInput: e => dispatch(newInput(e.target.value))
})

const CommandInputWrapper = connect(mapStateToProps, mapDispatchToProps)(InputLine)

export default CommandInputWrapper