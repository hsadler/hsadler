import { connect } from 'react-redux'
import App from '../components/app/app'
import { refocus, sendCommand } from '../actions'

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({
    refocus: () => dispatch(refocus()),
    sendCommand: cmd => dispatch(sendCommand(cmd)),
})

const AppWrapper = connect(mapStateToProps, mapDispatchToProps)(App)

export default AppWrapper