import { connect } from 'react-redux'
import Terminal from '../components/terminal/terminal'
import { sentCommandsSelector } from '../selectors'

const mapStateToProps = state => ({
    sentCommands: sentCommandsSelector(state),
})

const mapDispatchToProps = dispatch => ({})

const TerminalWrapper = connect(mapStateToProps, mapDispatchToProps)(Terminal)

export default TerminalWrapper