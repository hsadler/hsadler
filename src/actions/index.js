import ReactGA from 'react-ga'
import { push } from 'react-router-redux'
import { currentCommandSelector } from '../selectors'

const CLEAR_HISTORY = 'CLEAR_HISTORY'
const clearHistory = () => ({ type: CLEAR_HISTORY })

const NEW_INPUT = 'NEW_INPUT'
const newInput = input => ({ type: NEW_INPUT, input })

const SEND_COMMAND = 'SEND_COMMAND'
const sendCommand = currentCommand => ({ type: SEND_COMMAND, currentCommand })

const REFOCUS = 'REFOCUS'
const refocus = () => ({ type: REFOCUS })

const submitCommand = () => (dispatch, getState) => {
    const cmd = currentCommandSelector(getState()).trim()

    if (cmd.toLowerCase() === "cls" || cmd.toLowerCase() === "clear") {
        dispatch(clearHistory())
        dispatch(push('/'))
    } else {
        dispatch(sendCommand(cmd))
        dispatch(push(urlify(cmd)))
    }

    ReactGA.pageview(window.location.hash)
}

const urlify = cmd => `/${cmd.replace(/ /g, '/')}`

export {
    CLEAR_HISTORY,
    NEW_INPUT,
    REFOCUS,
    SEND_COMMAND,
    newInput,
    refocus,
    sendCommand,
    submitCommand,
}