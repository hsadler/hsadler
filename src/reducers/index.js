import { combineReducers } from 'redux'
import shortid from 'shortid'
import { CLEAR_HISTORY, NEW_INPUT, REFOCUS, SEND_COMMAND } from '../actions'

const buildCommand = (command, hidden) => ({ id: shortid.generate(), command, hidden })

const currentCommand = (state = "", action) => {
    switch(action.type) {
        case CLEAR_HISTORY:
            return ""
        case NEW_INPUT:
            return action.input
        case SEND_COMMAND:
            return ""
        default:
            return state
    }
}

const refocusCount = (state = 0, action) => {
    switch(action.type) {
        case REFOCUS:
            return state + 1
        default:
            return state
    }
}

const sentCommands = (state = [buildCommand("splash", true)], action) => {
    switch(action.type) {
        case CLEAR_HISTORY:
            return []
        case SEND_COMMAND:
            return [...state, buildCommand(action.currentCommand, false)]
        default:
            return state
    }
}

export default {
    currentCommand,
    refocusCount,
    sentCommands,
}